<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Продукт</title>
    <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
            crossorigin="anonymous"
    />
    <link rel="stylesheet" href="Classes/style.css">
</head>
<body>
<jsp:include page="header.jsp"/>

<container>
<div class="card-group product-card">
    <div class="card mt-5">
        <div class="card-body">
            <img
                    src="${product.url}"
                    alt="${product.url}"
                    class="mx-auto mt-3"
            />
        </div>
    </div>

    <div class="card mt-5">
        <div class="card-header">
            <p class="card-title product-name">
                <strong>${product.name}</strong>
            </p>
        </div>
        <div class="card-body">
            <p class="card-title product-description">
                ${product.description}
            </p>
        </div>
        <div class="card-footer">
            <p class="card-title product-price">
                <strong>${product.price}</strong>
            </p>
            <form action="${pageContext.request.contextPath}/controller" method="POST">
                <input type="hidden" name="id" value="${product.id}">
                <input type="hidden" name="toPage" value="product">
                <input type="hidden" name="cartAction" value="add">
                <button type="submit" class="btn btn-primary add-btn" name="controller"
                        value="cart">${messages.getString("productPage.orderButton")}
                </button>
            </form>
        </div>
    </div>
</div>
</container>

<script
        src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"
></script>
<script
        src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
        crossorigin="anonymous"
></script>
</body>
</html>
