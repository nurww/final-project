<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Регистрация</title>
    <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
            crossorigin="anonymous"
    />
    <link href="style.css" rel="stylesheet">
</head>
<body class="mx-auto">
<div class="card login-card mx-auto mt-5" style="width: 25rem">
    <div class="card-header bg-secondary d-flex justify-content-between">
        <img class="logo" src="./img/logo.png" alt="Logo" width="150px"/>
        <form action="${pageContext.request.contextPath}/controller" method="GET">
            <input type="hidden" name="toPage" value="home">
            <button
                    type="submit"
                    class="btn-close"
                    aria-label="Close"
                    name="controller"
                    value="redirect"
            ></button>
        </form>
    </div>
    <div class="card-body">
        <form
                action="${pageContext.request.contextPath}/controller"
                method="POST"
        >
            <div class="mb-3">
                <label for="email" class="form-label">${messages.getString("loginForm.login_email")}:</label>
                <input
                        type="email"
                        class="form-control"
                        name="email"
                        id="email"
                        placeholder="${messages.getString("loginForm.login_emailPlaceholder")}"
                        value="nurdaule@gmail.com"
                />
            </div>
            <div class="mb-3">
                <label for="password" class="form-label">${messages.getString("loginForm.login_password")}:</label>
                <input
                        type="password"
                        class="form-control"
                        name="password"
                        id="password"
                        placeholder="${messages.getString("loginForm.login_passwordPlaceholder")}"
                        value="12345"
                />
            </div>
            <input type="hidden" name="toPage" value="home">
            <input type="hidden" name="controller" value="login">
            <button type="submit" class="btn btn-primary" name="login" value="signIn">
                ${messages.getString("loginForm.login_signInButton")}
            </button>
            <button type="submit" class="btn btn-primary" name="login" value="signUp">
                ${messages.getString("loginForm.login_signUpButton")}
            </button>
        </form>
    </div>
</div>

<c:if test="${requestScope.error=='error'}">
    <div style="opacity: 1" class="toast align-items-center text-white bg-danger border-0" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="d-flex">
            <div class="toast-body">
                Wrong data or user exists
            </div>
            <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
    </div>
</c:if>

<script
        src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"
></script>
<script
        src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
        crossorigin="anonymous"
></script>
</body>
</html>
