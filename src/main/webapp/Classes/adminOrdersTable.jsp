<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<table class="table table-hover table-database display-none">
    <thead>
    <tr>
        <th class="w-25" scope="col">${messages.getString("adminPage.ordersTableUserEmail")}</th>
        <th class="w-25" scope="col">${messages.getString("adminPage.ordersTableName")}</th>
        <th class="w-10" scope="col">${messages.getString("adminPage.ordersTablePrice")}</th>
        <th class="w-10" scope="col">${messages.getString("adminPage.ordersTableAmount")}</th>
        <th class="w-10" scope="col">${messages.getString("adminPage.ordersTableTotalPrice")}</th>
        <th class="w-10" scope="col">${messages.getString("adminPage.ordersTableCategory")}</th>
        <th class="w-10" scope="col">${messages.getString("adminPage.tableRemove")}</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="order" items="${orderList}">
        <tr>
            <td>${order.email}</td>
            <td>${order.productName}</td>
            <td>${order.price}</td>
            <td>${order.amount}</td>
            <td>${order.totalPrice}</td>
            <td>${order.category}</td>
            <td>
                <form
                        action="${pageContext.request.contextPath}/controller"
                        method="POST"
                        class="d-flex flex-row flex-nowrap"
                >
                    <input type="hidden" name="toPage" value="admin">
                    <input type="hidden" name="orderId" value="${order.id}">
                    <input type="hidden" name="admin" value="removeOrder">
                    <button
                            type="submit"
                            class="btn btn-danger"
                            name="controller"
                            value="adminOrderTable"
                    >
                        X
                    </button>
                </form>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
