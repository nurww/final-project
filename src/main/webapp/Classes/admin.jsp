<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Админ</title>
    <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
            crossorigin="anonymous"
    />
    <link rel="stylesheet" href="Classes/style.css">
</head>
<body>
<jsp:include page="header.jsp"/>

<div class="card-group">
    <div class="card mt-5 w-15 narrow">
        <div class="card-body">
            <div class="list-group list-settings">
                <a href="#"
                   class="list-group-item list-group-item-action active">
                    ${messages.getString("adminPage.productsTableLink")}
                </a>
                <a href="#"
                   class="list-group-item list-group-item-action">
                    ${messages.getString("adminPage.ordersTableLink")}
                </a>
                <a href="#"
                   class="list-group-item list-group-item-action">
                    ${messages.getString("adminPage.usersTableLink")}
                </a>
            </div>
        </div>
    </div>

    <div class="card mt-5 w-85">
        <div class="card-header">
            <p class="card-title product-name">
                <strong class="table-title"></strong>
            </p>
        </div>
        <div class="card-body table-responsive">
            <jsp:include page="adminProductsTable.jsp"/>
            <jsp:include page="adminOrdersTable.jsp"/>
            <jsp:include page="adminUsersTable.jsp"/>
        </div>
    </div>
</div>

<script
        src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"
></script>
<script
        src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
        crossorigin="anonymous"
></script>

<script>
    let links = $('.list-group-item-action');
    let tables = $(".table");
    let tableTitle = $(".table-title");

    tableTitle.text($(".list-group-item-action.active").text());

    links.each((idx, el) => {
        $(el).click(() => {
            links.removeClass("active");
            tables.addClass("display-none");
            $(el).addClass("active");
            $(tables[idx]).removeClass("display-none");

            tableTitle.text($(el).text());

        });
    });
</script>

</body>
</html>
