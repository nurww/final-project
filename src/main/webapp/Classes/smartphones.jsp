<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Главная</title>
    <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
            crossorigin="anonymous"
    />
    <link rel="stylesheet" href="Classes/style.css">
</head>
<body>
<jsp:include page="header.jsp"/>

<div class="container mt-5">
    <div class="row row-cols-2 row-cols-lg-5 g-2 g-lg-3">
        <c:forEach var="smartphone" items="${smartphones}">
            <div class="col">
                <div class="card product-card">
                    <form action="${pageContext.request.contextPath}/controller" method="POST">
                        <input type="hidden" name="id" value="${product.id}">
                        <input type="hidden" name="toPage" value="product">
                        <button type="submit" class="btn" name="controller" value="product">
                            <img
                                    src="${smartphone.url}"
                                    alt="${smartphone.url}"
                                    class="mx-auto mt-3"
                            />
                            <div class="card-body">
                                <p class="card-title">
                                    <strong>${smartphone.name}</strong>
                                </p>
                            </div>
                        </button>
                    </form>
                    <div class="card-footer d-flex justify-content-between">
                        <form action="${pageContext.request.contextPath}/controller" method="POST">
                            <input type="hidden" name="id" value="${smartphone.id}">
                            <input type="hidden" name="toPage" value="smartphones">
                            <input type="hidden" name="cartAction" value="add">
                            <button type="submit" class="btn btn-primary add-to-cart" name="controller"
                                    value="cart">${messages.getString("homePage.addToCartButton")}
                            </button>
                        </form>
                        <span>${smartphone.price}</span>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
</div>

<script
        src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"
></script>
<script
        src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
        crossorigin="anonymous"
></script>
</body>
</html>