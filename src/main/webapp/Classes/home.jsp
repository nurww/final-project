<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<div class="container mt-5">
    <div class="row row-cols-2 row-cols-lg-5 g-2 g-lg-3">
        <c:forEach var="product" items="${products}">
            <div class="col">
                <div class="card product-card">
                    <form action="${pageContext.request.contextPath}/controller" method="POST">
                        <input type="hidden" name="id" value="${product.id}">
                        <input type="hidden" name="toPage" value="product">
                        <button type="submit" class="btn" name="controller" value="product">
                            <img
                                    src="${product.url}"
                                    alt="${product.url}"
                                    class="mx-auto mt-3"
                            />
                            <div class="card-body">
                                <p class="card-title">
                                    <strong>${product.name}</strong>
                                </p>
                            </div>
                        </button>
                    </form>
                    <div class="card-footer d-flex justify-content-between">
                        <form action="${pageContext.request.contextPath}/controller" method="POST">
                            <input type="hidden" name="id" value="${product.id}">
                            <input type="hidden" name="toPage" value="home">
                            <input type="hidden" name="cartAction" value="add">
                            <button type="submit" class="btn btn-primary add-to-cart" name="controller"
                                    value="cart">${messages.getString("homePage.addToCartButton")}
                            </button>
                        </form>
                        <span>${product.price}</span>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
</div>


<%--
<div class="container mt-5">
    <div class="row row-cols-2 row-cols-lg-5 g-2 g-lg-3">
        <c:forEach var="product" items="${products}">
            <a href="${pageContext.request.contextPath}/controller?controller=redirect&id=${product.id}&toPage=product">
                <div class="col">
                    <div class="card product-card">
                        <img
                                src="${product.url}"
                                alt="${product.url}"
                                class="mx-auto mt-3"
                        />
                        <div class="card-body">
                            <p class="card-title">
                                <strong>${product.name}</strong>
                            </p>
                        </div>
                        <div class="card-footer d-flex justify-content-between">
                            <form action="${pageContext.request.contextPath}/controller" method="POST">
                                <input type="hidden" name="id" value="${product.id}">
                                <input type="hidden" name="toPage" value="home">
                                <input type="hidden" name="cartAction" value="add">
                                <button type="submit" class="btn btn-primary add-to-cart" name="controller"
                                        value="cart">${messages.getString("homePage.addToCartButton")}
                                </button>
                            </form>
                            <span>${product.price}</span>
                        </div>
                    </div>
                </div>
            </a>
        </c:forEach>
    </div>
</div>
--%>
