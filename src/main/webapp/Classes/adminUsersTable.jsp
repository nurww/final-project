<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<table class="table table-hover table-database display-none">
    <thead>
    <tr>
        <th class="w-25" scope="col">${messages.getString("adminPage.usersTableName")}</th>
        <th class="w-25" scope="col">${messages.getString("adminPage.usersTableEmail")}</th>
        <th class="w-25" scope="col">${messages.getString("adminPage.usersTableRole")}</th>
        <th class="w-25" scope="col">${messages.getString("adminPage.tableRemove")}</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="customer" items="${customerList}">
        <tr>
            <td>${customer.name}</td>
            <td>${customer.email}</td>
            <td>${customer.role}</td>
            <td>
                <form
                        action="${pageContext.request.contextPath}/controller"
                        method="POST"
                        class="d-flex flex-row flex-nowrap"
                >
                    <input type="hidden" name="toPage" value="admin">
                    <input type="hidden" name="userId" value="${customer.id}">
                    <input type="hidden" name="admin" value="removeUser">
                    <button
                            type="submit"
                            class="btn btn-danger"
                            name="controller"
                            value="adminUserTable"
                    >
                        X
                    </button>
                </form>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>