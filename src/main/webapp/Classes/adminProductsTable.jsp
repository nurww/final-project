<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<table class="table table-hover table-database">
    <thead>
    <tr>
        <th class="w-25" scope="col">${messages.getString("adminPage.productsTableName")}</th>
        <th class="w-10" scope="col">${messages.getString("adminPage.productsTablePrice")}</th>
        <th class="w-10" scope="col">${messages.getString("adminPage.productsTableAmount")}</th>
        <th class="w-10" scope="col">${messages.getString("adminPage.productsTableCategory")}</th>
        <th class="w-10" scope="col">${messages.getString("adminPage.productsTableManufacturer")}</th>
        <th class="w-25" scope="col">${messages.getString("adminPage.productsTableDescription")}</th>
        <th class="w-10" scope="col">${messages.getString("adminPage.tableRemove")}</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <form
                action="${pageContext.request.contextPath}/controller"
                method="POST"
        >
            <td>
                <input
                        type="text"
                        class="form-control"
                        name="productName"
                        required/>
            </td>
            <td>
                <input
                        type="number"
                        class="form-control"
                        name="productPrice"
                        required/>
            </td>
            <td>
                <input
                        type="number"
                        class="form-control"
                        name="productAmount"
                        required/>
            </td>
            <td>
                <div class="input-group mb-3">
                    <select class="form-select form-control" name="productCategory" required>
                        <option selected
                                value="Smartphone">${messages.getString("adminPage.productsTableCategorySmartphone")}</option>
                        <option value="Notebook">${messages.getString("adminPage.productsTableCategoryLaptop")}</option>
                    </select>
                </div>
            </td>
            <td>
                <div class="input-group mb-3">
                    <select class="form-select form-control" name="productManufacturer" required>
                        <option selected value="Apple">Apple</option>
                        <option value="Samsung">Samsung</option>
                        <option value="Huawei">Huawei</option>
                        <option value="Asus">Asus</option>
                        <option value="Lenovo">Lenovo</option>
                        <option value="Dell">Dell</option>
                    </select>
                </div>
            </td>
            <td>
            <textarea
                    class="form-control"
                    name="productDescription"
            ></textarea>
            </td>
            <td class="d-flex">
                <input type="hidden" name="toPage" value="admin">
                <input type="hidden" name="admin" value="addProduct">
                <button
                        type="submit"
                        class="btn btn-success"
                        name="controller"
                        value="adminProductTable"
                >
                    V
                </button>
            </td>
        </form>
    </tr>
    <c:forEach var="product" items="${products}">
        <tr>
            <td>${product.name}</td>
            <td>${product.price}</td>
            <td>${product.amount}</td>
            <td>${product.category}</td>
            <td>${product.manufacturer}</td>
            <td class="overflow-auto">
                <div class="td-text-height">
                        ${product.description}
                </div>
            </td>
            <td class="d-flex">
                <form
                        action="${pageContext.request.contextPath}/controller"
                        method="POST"
                        class="d-flex flex-row flex-nowrap"
                >
                    <input type="hidden" name="toPage" value="admin">
                    <input type="hidden" name="productId" value="${product.id}">
                    <input type="hidden" name="admin" value="removeProduct">
                    <button
                            type="submit"
                            class="btn btn-danger"
                            name="controller"
                            value="adminProductTable"
                    >
                        X
                    </button>
                </form>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>