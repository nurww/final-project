<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Корзина</title>
    <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
            crossorigin="anonymous"
    />
    <link rel="stylesheet" href="Classes/style.css">
</head>
<body>
<jsp:include page="header.jsp"/>

<div class="container cart">
    <div class="row justify-content-between">
        <div class="card mt-5 w-66 p-4">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th class="w-50" scope="col">${messages.getString("cartPage.tableProducts")}</th>
                    <th class="w-25" scope="col">${messages.getString("cartPage.tableAmount")}</th>
                    <th class="w-25" scope="col">${messages.getString("cartPage.tablePrice")}</th>
                    <th class="w-10" scope="col">${messages.getString("cartPage.tableDeleteButton")}</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="product" items="${productsInCart}">
                    <tr>
                        <td>
                            <div class="container">
                                <div class="row align-items-start">
                                    <div class="col-sm-3">
                                        <img class="mx-auto" src="${product.url}" alt="${product.url}">
                                    </div>
                                    <div class="col">
                                            ${product.name}
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>${product.amount}</td>
                        <td>${product.price * product.amount}</td>
                        <td>
                            <form action="${pageContext.request.contextPath}/controller" method="POST">
                                <input type="hidden" name="id" value="${product.id}">
                                <input type="hidden" name="toPage" value="cart">
                                <input type="hidden" name="cartAction" value="remove">
                                <button type="submit" class="btn btn-danger" name="controller" value="cart">X</button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

        <div class="card mt-5 w-33 p-4">
            <div class="container">
                <div class="row justify-content-between mb-2">
                    <div class="col-6">${totalAmount} ${messages.getString("cartPage.checkOutText")}</div>
                    <div class="col-6 text-end">${totalPrice}</div>
                </div>
                <div class="row justify-content-between mb-2">
                    <c:choose>
                        <c:when test="${sessionScope.account=='guest'}">
                            <form action="${pageContext.request.contextPath}/controller" method="GET">
                                <input type="hidden" name="toPage" value="login">
                                <button type="submit" class="btn btn-primary" name="controller" value="redirect">${messages.getString("cartPage.checkOutButton")}</button>
                            </form>
                        </c:when>
                        <c:otherwise>
                            <form action="${pageContext.request.contextPath}/controller" method="POST">
                                <input type="hidden" name="toPage" value="cart">
                                <button type="submit" class="btn btn-primary" name="controller" value="checkOut">${messages.getString("cartPage.checkOutButton")}</button>
                            </form>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
    </div>
</div>

<script
        src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"
></script>
<script
        src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
        crossorigin="anonymous"
></script>
</body>
</html>
