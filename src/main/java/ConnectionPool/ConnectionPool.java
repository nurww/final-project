package ConnectionPool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ConnectionPool {
    static List<Connection> availableConnections = new ArrayList();
    private static ConnectionPool instance;

    public ConnectionPool() {
        this.initializeConnectionPool();
    }

    public static ConnectionPool getInstance() {
        ConnectionPool localInstance = instance;
        if (localInstance == null) {
            synchronized(ConnectionPool.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new ConnectionPool();
                }
            }
        }

        return localInstance;
    }

    private void initializeConnectionPool() {
        while(!this.checkIfConnectionPoolIsFull()) {
            try {
                availableConnections.add(this.createNewConnectionForPool());
            } catch (SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

    }

    private boolean checkIfConnectionPoolIsFull() {
        int MAX_POOL_SIZE = Configuration.DB_POLL_SIZE;
        return availableConnections.size() >= MAX_POOL_SIZE;
    }

    private Connection createNewConnectionForPool() throws ClassNotFoundException, SQLException {
        Class.forName(Configuration.DB_DRIVER);
        return DriverManager.getConnection(Configuration.DB_URL, Configuration.DB_USER, Configuration.DB_PASSWORD);
    }

    public Connection getConnectionFromPool() {
        Connection connection = null;
        if (availableConnections.size() > 0) {
            connection = (Connection)availableConnections.get(0);
            availableConnections.remove(0);
        }

        return connection;
    }

    public void returnConnectionToPool(Connection connection) {
        availableConnections.add(connection);
    }
}
