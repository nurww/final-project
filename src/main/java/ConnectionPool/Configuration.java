package ConnectionPool;

public class Configuration {
    DBResourceManager dbResourceManager = new DBResourceManager();
    public static String DB_DRIVER;
    public static String DB_URL;
    public static String DB_USER;
    public static String DB_PASSWORD;
    public static int DB_POLL_SIZE;
    private static final Configuration configuration = new Configuration();

    public Configuration() {
        DB_DRIVER = this.dbResourceManager.getValue("db.driver");
        DB_URL = this.dbResourceManager.getValue("db.url");
        DB_USER = this.dbResourceManager.getValue("db.user");
        DB_PASSWORD = this.dbResourceManager.getValue("db.password");
        DB_POLL_SIZE = Integer.parseInt(this.dbResourceManager.getValue("db.poolsize"));
    }
}
