package ConnectionPool;

import java.sql.Connection;

public class DataSource {
    public static ConnectionPool pool = ConnectionPool.getInstance();

    public DataSource() {
    }

    public static Connection getConnection() {
        return pool.getConnectionFromPool();
    }

    public static void returnConnection(Connection connection) {
        pool.returnConnectionToPool(connection);
    }
}
