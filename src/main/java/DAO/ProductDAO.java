package DAO;

import ConnectionPool.DataSource;
import Entity.Product;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ProductDAO extends DAO<Product> {
    public ProductDAO() {
    }

    public Product getById(int id) {
        Product res = null;

        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM store_product WHERE id=" + id + ";");

            while (rs.next()) {
                res = new Product(
                        id,
                        rs.getString("name"),
                        rs.getString("price"),
                        rs.getString("category"),
                        rs.getString("manufacturer"),
                        rs.getString("description"),
                        rs.getString("amount"),
                        this.getUrlByProductId(id)
                );
            }

            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;
    }

    public void update(Product product) {
        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            String query = "UPDATE store_product SET name='" + product.getName()
                    + "',price='" + product.getPrice()
                    + "',category='" + product.getCategory()
                    + "',manufacturer='" + product.getManufacturer()
                    + "',description='" + product.getDescription()
                    + "',amount='" + product.getAmount()
                    + "',url='" + product.getUrl()
                    + "'  WHERE id=" + product.getId() + ";";
            stm.executeUpdate(query);
            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Product> getAll() {
        ArrayList<Product> res = new ArrayList<>();

        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM store_product;");

            while (rs.next()) {
                res.add(
                        new Product(
                                rs.getInt("id"),
                                rs.getString("name"),
                                rs.getString("price"),
                                rs.getString("category"),
                                rs.getString("manufacturer"),
                                rs.getString("description"),
                                rs.getString("amount"),
                                this.getUrlByProductId(rs.getInt("id")
                                )
                        )
                );
            }
            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;
    }

    public ArrayList<Product> getAllSmartphones() {
        ArrayList<Product> res = new ArrayList<>();

        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM store_product WHERE category='Smartphone';");

            while (rs.next()) {
                res.add(
                        new Product(
                                rs.getInt("id"),
                                rs.getString("name"),
                                rs.getString("price"),
                                rs.getString("category"),
                                rs.getString("manufacturer"),
                                rs.getString("description"),
                                rs.getString("amount"),
                                this.getUrlByProductId(rs.getInt("id")
                                )
                        )
                );
            }
            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;
    }

    public ArrayList<Product> getAllNotebooks() {
        ArrayList<Product> res = new ArrayList<>();

        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM store_product WHERE category='Notebook';");

            while (rs.next()) {
                res.add(
                        new Product(
                                rs.getInt("id"),
                                rs.getString("name"),
                                rs.getString("price"),
                                rs.getString("category"),
                                rs.getString("manufacturer"),
                                rs.getString("description"),
                                rs.getString("amount"),
                                this.getUrlByProductId(rs.getInt("id")
                                )
                        )
                );
            }
            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;
    }

    public void insert(Product product) {
        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            String query = "" +
                    "INSERT INTO store_product (name,price,category,manufacturer,description,amount) " +
                    "VALUES ('" + product.getName()
                    + "','" + product.getPrice()
                    + "','" + product.getCategory()
                    + "','" + product.getManufacturer()
                    + "','" + product.getDescription()
                    + "','" + product.getAmount() + "');";
            stm.executeUpdate(query);
            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(int id) {
        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            String query = "DELETE FROM store_product WHERE id=" + id + " ;";
            stm.executeUpdate(query);
            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(Product product) {
        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            String query = "DELETE FROM store_product WHERE id=" + product.getId() + " ;";
            stm.executeUpdate(query);
            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getUrlByProductId(int id) throws SQLException {
        String res = "";
        Connection conn = DataSource.getConnection();
        Statement stm = conn.createStatement();
        ResultSet rs = stm.executeQuery("SELECT url FROM store.store_image AS i " +
                "WHERE i.id = " +
                "(SELECT imageID FROM store.store_product WHERE id = " + id + ");");

        while (rs.next()) {
            res = rs.getString("url");
        }

        DataSource.returnConnection(conn);
        return res;
    }
}
