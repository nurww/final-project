package DAO;

import java.sql.SQLException;
import java.util.ArrayList;

public abstract class DAO<T> {
    public DAO() {
    }

    public abstract void insert(T t) throws SQLException, ClassNotFoundException;

    public abstract T getById(int id) throws SQLException, ClassNotFoundException;

    public abstract void update(T t) throws SQLException, ClassNotFoundException;

    public abstract void delete(int id) throws SQLException, ClassNotFoundException;

    public abstract ArrayList<T> getAll() throws SQLException, ClassNotFoundException;
}
