package DAO;

import ConnectionPool.DataSource;
import Entity.Customer;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CustomerDAO extends DAO<Customer> {
    public CustomerDAO() {
    }

    public Customer getById(int id) {
        Customer res = null;

        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(
                    "SELECT * FROM store_customer WHERE id=" + id + ";");

            while (rs.next()) {
                res = new Customer(
                        id,
                        rs.getString("name"),
                        rs.getString("password"),
                        rs.getString("email"),
                        rs.getString("role")
                );
            }
            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;
    }

    public Customer getByEmail(String email) {
        Customer res = null;

        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(
                    "SELECT * FROM store_customer WHERE email='" + email + "';");

            while (rs.next()) {
                res = new Customer(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("password"),
                        rs.getString("email"),
                        rs.getString("role")
                );
            }
            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;
    }

    public String updateName(String email, String name) {
        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            String query = "UPDATE store_customer SET name='" + name + "'  WHERE email='" + email + "';";
            stm.executeUpdate(query);
            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return name;
    }

    public String updateEmail(String oldEmail, String newEmail) {
        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            String query = "UPDATE store_customer SET email='" + newEmail + "'  WHERE email='" + oldEmail + "';";
            stm.executeUpdate(query);
            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return newEmail;
    }

    public String updatePassword(String email, String password) {
        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            String query = "UPDATE store_customer SET password='" + password + "'  WHERE email='" + email + "';";
            stm.executeUpdate(query);
            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return password;
    }

    public void update(Customer customer) {
        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            String query = "UPDATE store_customer SET name='" + customer.getName()
                    + "',password='" + customer.getPassword()
                    + "',email='" + customer.getEmail()
                    + "',role='" + customer.getRole()
                    + "'  WHERE id=" + customer.getId() + ";";
            stm.executeUpdate(query);
            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public ArrayList<Customer> getAll() {
        ArrayList<Customer> res = new ArrayList<>();

        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM store_customer;");

            while (rs.next()) {
                res.add(
                        new Customer(
                                rs.getInt("id"),
                                rs.getString("name"),
                                rs.getString("password"),
                                rs.getString("email"),
                                rs.getString("role")
                        )
                );
            }

            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;
    }

    public void insert(Customer customer) {
        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            String query = "" +
                    "INSERT INTO store_customer (name,password,email,role)" +
                    "VALUES ('" + customer.getName() + "','"
                    + customer.getPassword() + "','"
                    + customer.getEmail() + "','"
                    + customer.getRole() + "');";
            stm.executeUpdate(query);
            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void delete(int id) {
        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            String query = "DELETE FROM store_customer WHERE id=" + id + ";";
            stm.executeUpdate(query);
            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int getIdByEmail(String email) {
        int res = 0;

        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM store_customer " +
                    "WHERE email = '" + email + "'");
            while (rs.next()) {
                res = rs.getInt("id");
            }
            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;
    }

    public boolean checkLogin(String email, String password) {
        boolean result = true;

        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM store_customer " +
                    "WHERE email = '" + email + "' AND password = '" + password + "'");
            result = rs.next();
            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean checkRegistration(String email) {
        boolean result = true;

        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM store_customer WHERE email = '" + email + "'");
            result = !rs.next();
            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }
}
