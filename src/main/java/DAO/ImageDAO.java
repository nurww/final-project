package DAO;

import ConnectionPool.DataSource;
import Entity.Image;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ImageDAO extends DAO<Image> {
    public ImageDAO() {
    }

    public Image getById(int id) {
        Image res = null;

        try {
            Connection conn = DataSource.getConnection();
            Statement stm = null;
            stm = conn.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM store_image WHERE id=" + id + ";");

            while (rs.next()) {
                res = new Image(
                        id,
                        rs.getString("type"),
                        rs.getString("category"),
                        rs.getString("url")
                );
            }

            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;
    }

    public void update(Image image) {
        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            String query = "UPDATE store_image SET type='" + image.getType()
                    + "',category='" + image.getCategory()
                    + "',url='" + image.getUrl()
                    + "'  WHERE id=" + image.getId() + ";";
            stm.executeUpdate(query);
            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public ArrayList<Image> getAll() {
        ArrayList<Image> res = new ArrayList<>();

        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM store_image;");

            while (rs.next()) {
                res.add(
                        new Image(
                                rs.getInt("id"),
                                rs.getString("type"),
                                rs.getString("category"),
                                rs.getString("url")
                        )
                );
            }

            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;
    }

    public void insert(Image image) {
        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            String query = "" +
                    "INSERT INTO store_image (type,category,url) VALUES ('"
                    + image.getType() + "','"
                    + image.getCategory() + "','"
                    + image.getUrl() + "');";
            stm.executeUpdate(query);
            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void delete(int id) {
        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            String query = "DELETE FROM store_image WHERE id=" + id + " ;";
            stm.executeUpdate(query);
            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
