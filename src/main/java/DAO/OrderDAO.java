package DAO;

import ConnectionPool.DataSource;
import Entity.Order;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class OrderDAO extends DAO<Order> {

    public OrderDAO() {
    }

    public Order getById(int id) {
        Order res = null;

        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM store_order WHERE id=" + id + ";");

            while (rs.next()) {
                res = new Order(
                        rs.getInt("id"),
                        rs.getInt("id_customer"),
                        rs.getInt("id_product"),
                        rs.getInt("amount")
                );
            }

            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;
    }

    public void update(Order order) {
        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            String query = "UPDATE store_order SET id_customer='" + order.getCustomerId()
                    + "',id_product='" + order.getProductId()
                    + "',amount='" + order.getAmount()
                    + "'  WHERE id=" + order.getId() + ";";
            stm.executeUpdate(query);
            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Order> getAll() {
        ArrayList<Order> res = new ArrayList<>();

        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();

            ResultSet rs = stm.executeQuery(
                    "SELECT o.id, o.id_customer, o.id_product, o.amount, c.email, p.name, p.price, (p.price * o.amount) AS totalPrice, p.category " +
                    "FROM store_order AS o INNER JOIN store_product AS p INNER JOIN store_customer AS c " +
                    "WHERE o.id_customer = c.id AND o.id_product = p.id;");

            Order o;
            while (rs.next()) {
                o = new Order(
                        rs.getInt("id"),
                        rs.getInt("id_customer"),
                        rs.getInt("id_product"),
                        rs.getInt("amount"),
                        rs.getString("email"),
                        rs.getString("name"),
                        rs.getInt("price"),
                        rs.getInt("totalPrice"),
                        rs.getString("category")
                );

                res.add(o);
            }
            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return res;
    }

    public void insert(Order order) {
        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            String query = "" +
                    "INSERT INTO store_order (id_customer,id_product, amount) " +
                    "VALUES ('" + order.getCustomerId()
                    + "','" + order.getProductId()
                    + "','" + order.getAmount() + "');";
            stm.executeUpdate(query);
            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(int id) {
        try {
            Connection conn = DataSource.getConnection();
            Statement stm = conn.createStatement();
            String query = "DELETE FROM store_order WHERE id=" + id + " ;";
            stm.executeUpdate(query);
            DataSource.returnConnection(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
