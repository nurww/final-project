package Controller;

import DAO.CustomerDAO;
import DAO.OrderDAO;
import DAO.ProductDAO;
import Entity.Customer;
import Entity.Order;
import Entity.Product;
import Services.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Controller extends HttpServlet {
    private final Redirect redirect = new Redirect();
    private static int totalPrice = 0;
    private static int totalAmount = 0;
    private static String toPage = "";
    private static ArrayList<Product> productsInCart = new ArrayList<>();
    private static ArrayList<Order> orderList = new ArrayList<>();
    private static ArrayList<Product> productsList;
    private static ArrayList<Product> smartphonesList;
    private static ArrayList<Product> notebooksList;
    private static ArrayList<Customer> customerList;

    public Controller() {
        ProductDAO productDAO = new ProductDAO();
        CustomerDAO customerDAO = new CustomerDAO();
        OrderDAO orderDAO = new OrderDAO();
        productsList = productDAO.getAll();
        smartphonesList = productDAO.getAllSmartphones();
        notebooksList = productDAO.getAllNotebooks();
        customerList = customerDAO.getAll();
        orderList = orderDAO.getAll();
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String controller = req.getParameter("controller");
        toPage = req.getParameter("toPage");

        switch (controller) {
            case "init":
                req.setAttribute("products", productsList);
                req.getSession().setAttribute("lang", "RU");
                req.getSession().setAttribute("account", "guest");
                ResourceBundle messages = ResourceBundle.getBundle("messages_ru");
                req.getSession().setAttribute("messages", messages);
                RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/index.jsp");
                dispatcher.forward(req, resp);
                break;
            case "redirect":
                this.redirect.execute(req, resp);
                break;
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String controller = req.getParameter("controller");
        toPage = req.getParameter("toPage");

        Service service = ServiceFactory.getService(controller);
        service.execute(req, resp);

        this.redirect.execute(req, resp);
    }

    public static String getToPage() {
        return toPage;
    }

    public static void setToPage(String toPage) {
        Controller.toPage = toPage;
    }

    public static int getTotalPrice() {
        return totalPrice;
    }

    public static void setTotalPrice(int totalPrice) {
        Controller.totalPrice = totalPrice;
    }

    public static int getTotalAmount() {
        return totalAmount;
    }

    public static void setTotalAmount(int totalAmount) {
        Controller.totalAmount = totalAmount;
    }

    public static ArrayList<Product> getProductsInCart() {
        return productsInCart;
    }

    public static void setProductsInCart(ArrayList<Product> productsInCart) {
        Controller.productsInCart = productsInCart;
    }

    public static ArrayList<Order> getOrderList() {
        return orderList;
    }

    public static void setOrderList(ArrayList<Order> orderList) {
        Controller.orderList = orderList;
    }

    public static ArrayList<Product> getProductsList() {
        return productsList;
    }

    public static void setProductsList(ArrayList<Product> productsList) {
        Controller.productsList = productsList;
    }

    public static ArrayList<Product> getSmartphonesList() {
        return smartphonesList;
    }

    public static void setSmartphonesList(ArrayList<Product> smartphonesList) {
        Controller.smartphonesList = smartphonesList;
    }

    public static ArrayList<Product> getNotebooksList() {
        return notebooksList;
    }

    public static void setNotebooksList(ArrayList<Product> notebooksList) {
        Controller.notebooksList = notebooksList;
    }

    public static ArrayList<Customer> getCustomerList() {
        return customerList;
    }

    public static void setCustomerList(ArrayList<Customer> customerList) {
        Controller.customerList = customerList;
    }
}
