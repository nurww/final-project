package Services;

import Controller.Controller;
import DAO.OrderDAO;
import Entity.Order;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class AdminOrderService implements Service {
    static Logger log = Logger.getLogger(AdminOrderService.class.getName());
    OrderDAO orderDAO = new OrderDAO();

    public AdminOrderService() {
    }

    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ArrayList<Order> orderList = Controller.getOrderList();
        int orderId = Integer.parseInt(req.getParameter("orderId"));
        Order order = orderDAO.getById(orderId);
        orderList.remove(order);
        Controller.setOrderList(orderList);
        orderDAO.delete(orderId);

        log.info(order.toString() + " removed to store_order table");

        Controller.setOrderList(orderList);
    }
}
