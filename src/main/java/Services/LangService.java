package Services;

import java.io.IOException;
import java.util.ResourceBundle;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LangService implements Service {
    public LangService() {
    }

    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String lang = req.getParameter("lang");
        String bundle = "";
        if (lang.equals("RU")) {
            bundle = "messages_ru";
        } else if (lang.equals("EN")) {
            bundle = "messages_en";
        }

        ResourceBundle messages = ResourceBundle.getBundle(bundle);
        req.getSession().setAttribute("lang", lang);
        req.getSession().setAttribute("messages", messages);
    }
}
