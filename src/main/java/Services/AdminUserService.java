package Services;

import Controller.Controller;
import DAO.CustomerDAO;
import Entity.Customer;
import Entity.Order;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class AdminUserService implements Service {
    static Logger log = Logger.getLogger(AdminUserService.class.getName());
    CustomerDAO customerDAO = new CustomerDAO();

    public AdminUserService() {
    }

    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ArrayList<Customer> customerList = Controller.getCustomerList();

        String user = (String) req.getSession().getAttribute("user");
        String admin = req.getParameter("admin");
        int userId = Integer.parseInt(req.getParameter("userId"));

        if (admin != null && admin.equals("removeUser")) {
            customerDAO.delete(userId);

            log.info(user + " removed from store_customer table");
        }

        req.setAttribute("customerList", customerList);

        Controller.setCustomerList(customerList);
    }
}
