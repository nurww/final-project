package Services;

public class ServiceFactory {
    public ServiceFactory() {
    }

    public static Service getService(String serviceType) {
        switch (serviceType) {
            case "profileChange":
                return new ProfileService();
            case "cart":
                return new CartService();
            case "lang":
                return new LangService();
            case "adminProductTable":
                return new AdminProductService();
            case "adminOrderTable":
                return new AdminOrderService();
            case "adminUserTable":
                return new AdminUserService();
            case "login":
                return new LoginService();
            case "logOut":
                return new LogoutService();
            case "checkOut":
                return new CheckOutService();
            case "product":
                return new ProductService();
            default:
                return null;
        }
    }
}
