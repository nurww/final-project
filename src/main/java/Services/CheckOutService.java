package Services;

import Controller.Controller;
import DAO.CustomerDAO;
import DAO.OrderDAO;
import DAO.ProductDAO;
import Entity.Order;
import Entity.Product;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CheckOutService implements Service {
    static Logger log = Logger.getLogger(CheckOutService.class.getName());
    CustomerDAO customerDAO = new CustomerDAO();
    ProductDAO productDAO = new ProductDAO();
    OrderDAO orderDAO = new OrderDAO();

    public CheckOutService() {
    }

    public void execute(HttpServletRequest req, HttpServletResponse resp) {
        ArrayList<Product> productsInCart = Controller.getProductsInCart();
        String email = (String) req.getSession().getAttribute("user");

        for (Product p : productsInCart) {
            orderDAO.insert(
                    new Order(
                            0,
                            customerDAO.getIdByEmail(email),
                            p.getId(),
                            p.getAmount()
                    )
            );
        }

        ArrayList<Order> orderList = orderDAO.getAll();
        Controller.setOrderList(orderList);

        log.info(orderList.toString() + " removed from store_customer table");
    }
}
