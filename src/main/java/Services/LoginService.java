package Services;

import Controller.Controller;
import DAO.CustomerDAO;
import Entity.Customer;
import org.apache.log4j.Logger;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginService implements Service {
    static Logger log = Logger.getLogger(LoginService.class.getName());
    private final CustomerDAO customerDAO = new CustomerDAO();

    public LoginService() {
    }

    public void execute(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String login = req.getParameter("login");
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        boolean checkLogin = this.customerDAO.checkLogin(email, password);
        boolean checkRegister = this.customerDAO.checkRegistration(email);

        if (login.equals("signIn") && checkLogin) {
            Customer customer = customerDAO.getByEmail(email);
            req.getSession().setAttribute("account", customer.getRole());
            req.getSession().setAttribute("user", email);

            log.info(email + " user signed in");
        } else if (login.equals("signUp") && checkRegister) {
            this.customerDAO.insert(new Customer(0, "", password, email, "customer"));
            Controller.setToPage("login");

            log.info(email + " user signed up");
        } else {
            req.setAttribute("error", "error");
            Controller.setToPage("login");
        }
    }
}