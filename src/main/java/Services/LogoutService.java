package Services;

import Controller.Controller;
import DAO.ProductDAO;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class LogoutService implements Service {
    static Logger log = Logger.getLogger(LogoutService.class.getName());

    public LogoutService() {
    }

    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = (String) req.getSession().getAttribute("user");
        req.getSession().setAttribute("account", "guest");
        req.getSession().setAttribute("user", null);

        Controller.setTotalPrice(0);
        Controller.setTotalAmount(0);
        Controller.setProductsInCart(new ArrayList<>());

        log.info(email + " logged out");
    }
}
