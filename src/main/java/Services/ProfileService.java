package Services;

import DAO.CustomerDAO;
import org.apache.log4j.Logger;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ProfileService implements Service {
    static Logger log = Logger.getLogger(ProfileService.class.getName());

    public ProfileService() {
    }

    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CustomerDAO customerDao = new CustomerDAO();
        String user = (String)req.getSession().getAttribute("user");
        String name = req.getParameter("name");
        String email = req.getParameter("email");
        String newEmail = email;
        String password = req.getParameter("password");
        String confirmPass = req.getParameter("confirmPass");
        if (!name.equals("")) {
            customerDao.updateName(user, name);
        }
        if ((!password.equals("") && !confirmPass.equals("")) & password.equals(confirmPass)) {
            customerDao.updatePassword(user, password);
        }
        if (!email.equals("")) {
            newEmail = customerDao.updateEmail(user, email);
        }

        req.getSession().setAttribute("user", newEmail);

        log.info(email + " user profile updated");
    }
}
