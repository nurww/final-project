package Services;

import Controller.Controller;
import DAO.ProductDAO;
import Entity.Product;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ProductService implements Service {
    ProductDAO productDAO = new ProductDAO();

    public ProductService() {
    }

    public void execute(HttpServletRequest req, HttpServletResponse resp) {
        int productId = Integer.parseInt(req.getParameter("id"));
        Product product = productDAO.getById(productId);
        req.setAttribute("product", product);
    }
}
