package Services;

import Controller.Controller;
import DAO.ProductDAO;
import Entity.Product;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class AdminProductService implements Service {
    static Logger log = Logger.getLogger(AdminProductService.class.getName());
    ProductDAO productDAO = new ProductDAO();

    public AdminProductService() {
    }

    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ArrayList<Product> productsList = Controller.getProductsList();

        String admin = req.getParameter("admin");

        if (admin.equals("addProduct")) {
            Product product = new Product(
                    0,
                    req.getParameter("productName"),
                    req.getParameter("productPrice"),
                    req.getParameter("productCategory"),
                    req.getParameter("productManufacturer"),
                    req.getParameter("productDescription"),
                    req.getParameter("productAmount"),
                    ""
            );
            this.productDAO.insert(product);

            log.info(product.toString() + " added to store_product table");
        } else if (admin.equals("removeProduct")) {
            int productId = Integer.parseInt(req.getParameter("productId"));
            Product product = productDAO.getById(productId);
            productsList.remove(product);
            productDAO.delete(productId);

            log.info(product.toString() + " removed from store_product table");
        }

        Controller.setProductsList(productsList);
    }
}