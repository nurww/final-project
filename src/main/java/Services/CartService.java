package Services;

import Controller.Controller;
import DAO.ProductDAO;
import Entity.Product;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CartService implements Service {
    private final ProductDAO productDAO = new ProductDAO();

    public CartService() {
    }

    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ArrayList<Product> productsInCart = Controller.getProductsInCart();

        int productId = Integer.parseInt(req.getParameter("id"));
        String cartAction = req.getParameter("cartAction");
        Product product = this.productDAO.getById(productId);
        int productIdx;
        int productAmount;

        if (cartAction.equals("add")) {
            if (productsInCart.contains(product)) {
                productIdx = productsInCart.indexOf(product);
                product = productsInCart.get(productIdx);
                productAmount = product.getAmount() + 1;
                product.setAmount(productAmount);
            } else {
                product.setAmount(1);
                productsInCart.add(product);
            }

            Controller.setTotalAmount(Controller.getTotalAmount() + 1);
            Controller.setTotalPrice(Controller.getTotalPrice() + product.getPrice());
        } else if (cartAction.equals("remove")) {
            productIdx = productsInCart.indexOf(product);
            product = productsInCart.get(productIdx);
            productAmount = product.getAmount() - 1;
            if (productAmount > 0) {
                product.setAmount(productAmount);
            } else {
                productsInCart.remove(product);
            }

            Controller.setTotalAmount(Controller.getTotalAmount() - 1);
            Controller.setTotalPrice(Controller.getTotalPrice() - product.getPrice());
        }

        req.setAttribute("totalAmount", Controller.getTotalAmount());
        req.setAttribute("totalPrice", Controller.getTotalPrice());
        req.setAttribute("product", product);

        Controller.setProductsInCart(productsInCart);
    }
}
