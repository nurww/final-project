package Services;

import Controller.Controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Redirect {
    public Redirect() {
    }

    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String toPage = Controller.getToPage();
        String redirect = "";
        switch(toPage) {
            case "login":
                redirect = "/Classes/login.jsp";
                break;
            case "product":
                redirect = "/Classes/product.jsp";
                break;
            case "profile":
                redirect = "/Classes/profile.jsp";
                break;
            case "cart":
                req.setAttribute("productsInCart", Controller.getProductsInCart());
                req.setAttribute("totalPrice", Controller.getTotalPrice());
                req.setAttribute("totalAmount", Controller.getTotalAmount());
                redirect = "/Classes/cart.jsp";
                break;
            case "home":
                req.setAttribute("products", Controller.getProductsList());
                redirect = "/index.jsp";
                break;
            case "smartphones":
                req.setAttribute("smartphones", Controller.getSmartphonesList());
                redirect = "/Classes/smartphones.jsp";
                break;
            case "notebooks":
                req.setAttribute("notebooks", Controller.getNotebooksList());
                redirect = "/Classes/notebooks.jsp";
                break;
            case "admin":
                req.setAttribute("products", Controller.getProductsList());
                req.setAttribute("orderList", Controller.getOrderList());
                req.setAttribute("customerList", Controller.getCustomerList());
                redirect = "/Classes/admin.jsp";
                break;
        }

        req.setAttribute("toPage", toPage);
        RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher(redirect);
        dispatcher.forward(req, resp);
    }
}
