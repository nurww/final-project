package Entity;

import java.util.Objects;

public class Order {
    int id;
    int productId;
    int customerId;
    String email;
    String productName;
    int price;
    int amount;
    int totalPrice;
    String category;

    public Order(int id, int customerId, int productId, int amount) {
        this.id = id;
        this.customerId = customerId;
        this.productId = productId;
        this.amount = amount;
    }

    public Order(int id, int customerId, int productId, int amount,
                 String email, String productName, int price, int totalPrice,
                 String category) {
        this.id = id;
        this.customerId = customerId;
        this.productId = productId;
        this.amount = amount;
        this.email = email;
        this.productName = productName;
        this.price = price;
        this.totalPrice = totalPrice;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProductName() {
        return this.productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getPrice() {
        return this.price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getAmount() {
        return this.amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getTotalPrice() {
        return this.totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", productId=" + productId +
                ", customerId=" + customerId +
                ", email='" + email + '\'' +
                ", productName='" + productName + '\'' +
                ", price=" + price +
                ", amount=" + amount +
                ", totalPrice=" + totalPrice +
                ", category='" + category + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return getId() == order.getId() && getProductId() == order.getProductId() && getCustomerId() == order.getCustomerId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getProductId(), getCustomerId());
    }
}
