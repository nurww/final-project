package Entity;

import java.util.Objects;

public class Image {
    int id;
    String type;
    String category;
    String url;

    public Image(int id, String type, String category, String url) {
        this.id = id;
        this.type = type;
        this.category = category;
        this.url = url;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String toString() {
        return "Image{id=" + this.id + ", type='" + this.type + '\'' + ", category='" + this.category + '\'' + ", url='" + this.url + '\'' + '}';
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (!(o instanceof Image)) {
            return false;
        } else {
            Image image = (Image)o;
            return this.getId() == image.getId() && Objects.equals(this.getType(), image.getType()) && Objects.equals(this.getCategory(), image.getCategory()) && Objects.equals(this.getUrl(), image.getUrl());
        }
    }

    public int hashCode() {
        return Objects.hash(new Object[]{this.getId(), this.getType(), this.getCategory(), this.getUrl()});
    }
}
