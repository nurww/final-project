package Entity;

import java.util.Objects;

public class Product {
    int id;
    String name;
    int price;
    String category;
    String manufacturer;
    String description;
    int amount;
    String url;
    int imageID;

    public Product(int id, String name, String price, String category, String manufacturer, String description, String amount, String url) {
        this.id = id;
        this.name = name;
        this.price = Integer.parseInt(price);
        this.category = category;
        this.manufacturer = manufacturer;
        this.description = description;
        this.amount = Integer.parseInt(amount);
        this.url = url;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return this.price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getAmount() {
        return this.amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getImageID() {
        return this.imageID;
    }

    public void setImageID(int imageID) {
        this.imageID = imageID;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String toString() {
        return '\'' + this.name + '\'' + " '" + this.price + '\'' + " '" + this.category + '\'' + " '" + this.manufacturer + '\'' + " '" + this.amount + '\'';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return getId() == product.getId() && getPrice() == product.getPrice() && getName().equals(product.getName()) && Objects.equals(getCategory(), product.getCategory()) && Objects.equals(getManufacturer(), product.getManufacturer());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getPrice(), getCategory(), getManufacturer());
    }
}
