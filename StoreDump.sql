-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: store
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `store_customer`
--

DROP TABLE IF EXISTS `store_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `store_customer` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) DEFAULT NULL,
  `password` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `role` varchar(15) DEFAULT 'customer',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store_customer`
--

LOCK TABLES `store_customer` WRITE;
/*!40000 ALTER TABLE `store_customer` DISABLE KEYS */;
INSERT INTO `store_customer` VALUES (11,'Nurdaulet','12345','nurdaule@gmail.com','admin');
/*!40000 ALTER TABLE `store_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store_image`
--

DROP TABLE IF EXISTS `store_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `store_image` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` varchar(25) NOT NULL DEFAULT '',
  `category` varchar(25) NOT NULL,
  `url` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store_image`
--

LOCK TABLES `store_image` WRITE;
/*!40000 ALTER TABLE `store_image` DISABLE KEYS */;
INSERT INTO `store_image` VALUES (2,'png','smartphone','img/smartphone/iphone-12-pro-blue-hero.png'),(3,'png','smartphone','img/smartphone/iphone-12-pro-gold-hero.png'),(4,'png','smartphone','img/smartphone/iphone-12-pro-graphite-hero.png'),(5,'png','smartphone','img/smartphone/iphone-12-pro-silver-hero.png'),(6,'jpg','smartphone','img/smartphone/Samsung-Galaxy-S20-Blue.jpg'),(7,'jpg','smartphone','img/smartphone/Samsung Galaxy S20 Gray.jpg'),(8,'jpg','smartphone','img/smartphone/Samsung Galaxy S20 Red.jpg'),(9,'jpg','smartphone','img/smartphone/Samsung Galaxy S20 Green.jpg'),(10,'jpg','smartphone','img/smartphone/Huawei P30 Breathing Crystal.jpg'),(11,'jpg','smartphone','img/smartphone/Huawei P30 Pro Breathing Crystal.jpg'),(12,'jpg','smartphone','img/smartphone/Huawei P40 Pro Black.jpg'),(13,'jpg','smartphone','img/smartphone/Huawei P40 Pro Silver Frost.jpg'),(14,'jpg','notebook','img/notebook/Apple MacBook Air 13 Retina 512 Space Gray 2020 (MVH22RU-A).jpg'),(15,'jpg','notebook','img/notebook/Apple MacBook Air Retina i5 1030NG7-8ГБ-512SSD-13.3-Mac OS Catalina-(MVH52RU-A).jpg'),(16,'jpg','notebook','img/notebook/Apple MacBook Pro Touch Bar Retina 2020 i5 1038NG7-16ГБ-512SSD-Intel Iris Graphics-13.0-Mac OS-(MWP42RU-A).jpg'),(17,'jpg','notebook','img/notebook/Asus ROG Strix G15 i7 10870H-16ГБ-1000SSD-RTX2060 6ГБ-15,6-DOS (G512LV-HN246).jpg'),(18,'jpg','notebook','img/notebook/Asus ZenBook Flip S UX371EA i7 1165G7-16ГБ- 1000SSD-13.3-Win10PRO-(UX371EA-HL046R).jpg'),(19,'jpg','notebook','img/notebook/Asus ZenBook S i7 1165G7-16ГБ-512SSD-13.9-Win10-(UX393EA-HK003T).jpg'),(20,'jpg','notebook','img/notebook/Dell Inspiron Gaming 5500 i7 10750H-16ГБ-1000SSD-RTX2060 6ГБ-15.6-Win10-(210-AVQN).jpg'),(21,'jpg','notebook','img/notebook/Dell XPS 13 9300 i7 1065G7-16ГБ1000SSD-13.4-Win10-(210-AUQY-A1).jpg'),(22,'jpg','notebook','img/notebook/Lenovo Legion Y740 i7 9750H-16ГБ-1000SSD-15.6-RTX2070 Max Q 8 ГБ-DOS-(81UH00BMRK).jpg'),(23,'jpg','notebook','img/notebook/Lenovo ThinkPad Carbon X1 14 i7 8650U-16ГБ-512HDD-14-Win10PRO-(20KGS4QG0J).jpg');
/*!40000 ALTER TABLE `store_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store_order`
--

DROP TABLE IF EXISTS `store_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `store_order` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `id_customer` int NOT NULL,
  `id_product` int NOT NULL,
  `amount` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store_order`
--

LOCK TABLES `store_order` WRITE;
/*!40000 ALTER TABLE `store_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `store_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store_product`
--

DROP TABLE IF EXISTS `store_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `store_product` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `price` int DEFAULT NULL,
  `category` varchar(30) DEFAULT NULL,
  `manufacturer` varchar(30) DEFAULT NULL,
  `description` varchar(1200) DEFAULT NULL,
  `amount` int DEFAULT NULL,
  `imageID` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `imageId_idx` (`imageID`),
  CONSTRAINT `imageId` FOREIGN KEY (`imageID`) REFERENCES `store_image` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=448 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store_product`
--

LOCK TABLES `store_product` WRITE;
/*!40000 ALTER TABLE `store_product` DISABLE KEYS */;
INSERT INTO `store_product` VALUES (1,'Apple iPhone 12 Pro Max 256GB Blue',729990,'Smartphone','Apple','Шестилинзовый объектив, Формат Apple ProRAW, Семилинзовый объектив, Устранение эффекта красных глаз, Режим таймера, Панорама, Цифровой зум 12x, Эффект боке, HDR, Геотегинг, Режим серийной съемки, Режим замедленной съёмки, Режим Smart HDR 3 с распознаванием сцен, Кинематографическая стабилизация видео (4K, 1080p и 720p), Видео «Таймлапс» в Ночном режиме, Портретный режим, True Tone с функцией Slow Sync, Распознавание сцен, Таймлапс, Портретное освещение, Технология Deep Fusion, Live Photos, Пятилинзовый объектив, Функция QuickTake, Ночной режим для портретов с применением сканера LiDAR, Оптический зум 5x, Ночной режим',27,2),(2,'Apple iPhone 12 Pro Max 256GB Gold',729990,'Smartphone','Apple','Шестилинзовый объектив, Формат Apple ProRAW, Семилинзовый объектив, Устранение эффекта красных глаз, Режим таймера, Панорама, Цифровой зум 12x, Эффект боке, HDR, Геотегинг, Режим серийной съемки, Режим замедленной съёмки, Режим Smart HDR 3 с распознаванием сцен, Кинематографическая стабилизация видео (4K, 1080p и 720p), Видео «Таймлапс» в Ночном режиме, Портретный режим, True Tone с функцией Slow Sync, Распознавание сцен, Таймлапс, Портретное освещение, Технология Deep Fusion, Live Photos, Пятилинзовый объектив, Функция QuickTake, Ночной режим для портретов с применением сканера LiDAR, Оптический зум 5x, Ночной режим',27,3),(3,'Apple iPhone 12 Pro Max 256GB Graphite',729990,'Smartphone','Apple','Шестилинзовый объектив, Формат Apple ProRAW, Семилинзовый объектив, Устранение эффекта красных глаз, Режим таймера, Панорама, Цифровой зум 12x, Эффект боке, HDR, Геотегинг, Режим серийной съемки, Режим замедленной съёмки, Режим Smart HDR 3 с распознаванием сцен, Кинематографическая стабилизация видео (4K, 1080p и 720p), Видео «Таймлапс» в Ночном режиме, Портретный режим, True Tone с функцией Slow Sync, Распознавание сцен, Таймлапс, Портретное освещение, Технология Deep Fusion, Live Photos, Пятилинзовый объектив, Функция QuickTake, Ночной режим для портретов с применением сканера LiDAR, Оптический зум 5x, Ночной режим',27,4),(4,'Apple iPhone 12 Pro Max 256GB Silver',729990,'Smartphone','Apple','Шестилинзовый объектив, Формат Apple ProRAW, Семилинзовый объектив, Устранение эффекта красных глаз, Режим таймера, Панорама, Цифровой зум 12x, Эффект боке, HDR, Геотегинг, Режим серийной съемки, Режим замедленной съёмки, Режим Smart HDR 3 с распознаванием сцен, Кинематографическая стабилизация видео (4K, 1080p и 720p), Видео «Таймлапс» в Ночном режиме, Портретный режим, True Tone с функцией Slow Sync, Распознавание сцен, Таймлапс, Портретное освещение, Технология Deep Fusion, Live Photos, Пятилинзовый объектив, Функция QuickTake, Ночной режим для портретов с применением сканера LiDAR, Оптический зум 5x, Ночной режим',27,5),(5,'Samsung Galaxy S20 128GB Gray',349990,'Smartphone','Samsung','Ночной режим, Оптический зум 3x, Режим замедленной съёмки, Режим серийной съемки, Режим смарт-сцен, Суперстабилизация, Портретный режим, HDR, Контроль экспозиции, Таймлапс, Геотегинг, Эффект боке, Панорама, Режим таймера',32,7),(6,'Samsung Galaxy S20 128GB Green',349990,'Smartphone','Samsung','Ночной режим, Оптический зум 3x, Режим замедленной съёмки, Режим серийной съемки, Режим смарт-сцен, Суперстабилизация, Портретный режим, HDR, Контроль экспозиции, Таймлапс, Геотегинг, Эффект боке, Панорама, Режим таймера',23,9),(7,'Samsung Galaxy S20 128GB Blue',349990,'Smartphone','Samsung','Ночной режим, Оптический зум 3x, Режим замедленной съёмки, Режим серийной съемки, Режим смарт-сцен, Суперстабилизация, Портретный режим, HDR, Контроль экспозиции, Таймлапс, Геотегинг, Эффект боке, Панорама, Режим таймера',31,6),(8,'Samsung Galaxy S20 128GB Red',349990,'Smartphone','Samsung','Ночной режим, Оптический зум 3x, Режим замедленной съёмки, Режим серийной съемки, Режим смарт-сцен, Суперстабилизация, Портретный режим, HDR, Контроль экспозиции, Таймлапс, Геотегинг, Эффект боке, Панорама, Режим таймера',28,8),(9,'Huawei P30 Pro Breathing Crystal',349990,'Smartphone','Huawei','Панорама, Оптический зум 3x, Режим серийной съемки, HUAWEI AIS, AI HDR+, Режим таймера, Цифровой зум 50x, AI HDR, Гибридный зум 10x, Режим замедленной съёмки, Тач-фокус, HDR, Master AI, Геотегинг, Обнаружение лица',37,11),(10,'Huawei P30 Breathing Crystal',249990,'Smartphone','Huawei','Обнаружение лица, Трёхкратное увеличение, Режим замедленной съёмки, Режим таймера, Эффект боке, Быстрый макияж, HDR, Обнаружение улыбки, Геотегинг, Панорама, Контроль экспозиции, Тач-фокус, Режим серийной съемки, Двухтоновая вспышка',29,10),(11,'Huawei P40 Pro Silver Frost',499990,'Smartphone','Huawei','Регулировка глубины резкости, Режим таймера, Быстрый макияж, HDR, Режим серийной съемки, Эффект боке, Селфи портрет, Распознавание лиц',15,13),(12,'Huawei P40 Pro Black',499990,'Smartphone','Huawei','Регулировка глубины резкости, Режим таймера, Быстрый макияж, HDR, Режим серийной съемки, Эффект боке, Селфи портрет, Распознавание лиц',22,12),(13,'Apple MacBook Air Retina i5 1030NG7 / 8ГБ / 512SSD / 13.3 / Mac OS Catalina / (MVH52RU/A)',730990,'Notebook','Apple','Клавиатура Magic Keyboard, Трекпад Force Touch, Датчик Touch ID, Чип безопасности Apple T2',18,15),(14,'Apple MacBook Pro Touch Bar Retina 2020 i5 1038NG7 / 16ГБ / 512SSD / Intel Iris Graphics / 13.0 / Mac OS / (MWP42RU/A)',899990,'Notebook','Apple','Touch Bar, Датчик Touch ID, Чип безопасности Apple T2, Клавиатура Magic Keyboard, Трекпад Force Touch',12,16),(15,'Apple MacBook Air 13\" Retina 512 Space Gray 2020 (MVH22RU/A)',730990,'Notebook','Apple','Датчик Touch ID, Чип безопасности Apple T2, Клавиатура Magic Keyboard, Трекпад Force Touch',13,14),(16,'Asus ZenBook S i7 1165G7 / 16ГБ / 512SSD / 13.9 / Win10 / (UX393EA-HK003T)',709990,'Notebook','Asus','11-ое поколение (Tiger Lake)',15,19),(17,'Asus ROG Strix G15 i7 10870H / 16ГБ / 1000SSD / RTX2060 6ГБ / 15,6 / DOS (G512LV-HN246)',569990,'Notebook','Asus','ASUS NumberPad, Система интеллектуального шумоподавления, Технология WiFi Stabilizer, Технология WiFi SmartConnect, Intel Evo, Сверхтонкая экранная рамка NanoEdge, Стилус в комплекте, NumberPad 2.0 (тачпад, совмещенный с цифровым клавиатурным блоком), MyASUS',15,17),(18,'Asus ZenBook Flip S UX371EA i7 1165G7 / 16ГБ / 1000SSD / 13.3 / Win10PRO / (UX371EA-HL046R)',799990,'Notebook','Asus','ASUS NumberPad, Система интеллектуального шумоподавления, Технология WiFi Stabilizer, Технология WiFi SmartConnect, Intel Evo, Сверхтонкая экранная рамка NanoEdge, Стилус в комплекте, NumberPad 2.0 (тачпад, совмещенный с цифровым клавиатурным блоком), MyASUS',16,18),(19,'Lenovo ThinkPad Carbon X1 14 i7 8650U / 16ГБ / 512HDD / 14 / Win10PRO / (20KGS4QG0J)',799990,'Notebook','Lenovo','',17,23),(20,'Lenovo Legion Y740 i7 9750H / 16ГБ / 1000SSD / 15.6 / RTX2070 Max Q 8 ГБ / DOS / (81UH00BMRK)',843990,'Notebook','Lenovo','',13,22),(21,'Dell XPS 13 9300 i7 1065G7 / 16ГБ / 1000SSD / 13.4 / Win10 / (210-AUQY-A1)',999990,'Notebook','Dell','Технология Dell Eyesafe, Встроенный сенсорный экран',10,21),(22,'Dell Inspiron Gaming 5500 i7 10750H / 16ГБ / 1000SSD / RTX2060 6ГБ / 15.6 / Win10 / (210-AVQN)',799990,'Notebook','Dell','Термальное охлаждение',12,20);
/*!40000 ALTER TABLE `store_product` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-05  8:26:13
