<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-secondary px-5">
        <img class="logo" src="./img/logo.png" alt="logo"/>
        <button
                class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarTogglerDemo02"
                aria-controls="navbarTogglerDemo02"
                aria-expanded="false"
                aria-label="Toggle navigation"
        >
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor02">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page"
                       href="${pageContext.request.contextPath}/controller?controller=redirect&toPage=home">
                        ${messages.getString("homePage.home")}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"
                       href="${pageContext.request.contextPath}/controller?controller=redirect&toPage=smartphones">
                        ${messages.getString("homePage.smartphones")}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"
                       href="${pageContext.request.contextPath}/controller?controller=redirect&toPage=notebooks">
                        ${messages.getString("homePage.notebooks")}
                    </a>
                </li>
            </ul>
            <div class="d-flex align-items-center">
                <c:if test="${sessionScope.account=='admin'}">
                    <a href="${pageContext.request.contextPath}/controller?controller=redirect&toPage=admin">
                        <img src="./img/admin.svg" alt="admin"/>
                    </a>
                </c:if>
                <c:choose>
                    <c:when test="${sessionScope.account=='guest'}">
                        <a href="${pageContext.request.contextPath}/controller?controller=redirect&toPage=login">
                            <img src="./img/user.svg" alt="user"/>
                        </a>
                    </c:when>
                    <c:otherwise>
                        <a href="${pageContext.request.contextPath}/controller?controller=redirect&toPage=profile">
                            <img src="./img/user.svg" alt="user"/>
                        </a>
                    </c:otherwise>
                </c:choose>
                <a href="${pageContext.request.contextPath}/controller?controller=redirect&toPage=cart">
                    <img src="./img/cart.svg" alt="cart"/>
                </a>
                <div class="dropdown dropstart mx-2">
                    <button
                            class="btn btn-light dropdown-toggle"
                            type="button"
                            id="dropdownMenuButton"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                    >
                        ${messages.getString("langSelect.activeLang")}
                    </button>
                    <div
                            class="dropdown-menu"
                            aria-labelledby="dropdownMenuButton"
                            x-placement="bottom-start"
                    >
                        <form
                                action="${pageContext.request.contextPath}/controller"
                                method="POST"
                        >
                            <input type="hidden" name="lang" value="RU">
                            <input type="hidden" name="toPage" value="${toPage}">
                            <button
                                    class="form-control dropdown-item"
                                    name="controller"
                                    value="lang"
                                    type="submit">
                                ${messages.getString("langSelect.Ru")}
                            </button>
                        </form>
                        <form
                                action="${pageContext.request.contextPath}/controller"
                                method="POST"
                        >
                            <input type="hidden" name="lang" value="EN">
                            <input type="hidden" name="toPage" value="${toPage}">
                            <button
                                    class="form-control dropdown-item"
                                    name="controller"
                                    value="lang"
                                    type="submit">
                                ${messages.getString("langSelect.En")}
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</header>


