<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Личный кабинет</title>
    <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
            crossorigin="anonymous"
    />
    <link href="Classes/style.css" rel="stylesheet">
</head>
<body>
<jsp:include page="header.jsp"/>

<div class="card mt-5 mx-auto" style="width: 25rem">
    <div class="card-header bg-secondary">
        <img class="logo" src="./img/logo.png" alt="Logo" width="150px"/>
    </div>
    <div class="card-body">
        <form
                action="${pageContext.request.contextPath}/controller"
                method="POST"
        >
            <div class="mb-3">
                <label for="name" class="form-label">
                    ${messages.getString("profilePage.name")}
                </label>
                <input
                        type="text"
                        class="form-control"
                        name="name"
                        id="name"
                        placeholder="${messages.getString("profilePage.namePlaceholder")}"
                />
            </div>
            <div class="mb-3">
                <label for="email" class="form-label">${messages.getString("profilePage.email")}:</label>
                <input
                        type="email"
                        class="form-control"
                        name="email"
                        id="email"
                        placeholder="${messages.getString("profilePage.emailPlaceholder")}"
                />
            </div>
            <div class="mb-3">
                <label for="password" class="form-label">${messages.getString("profilePage.password")}:</label>
                <input
                        type="password"
                        class="form-control"
                        name="password"
                        id="password"
                        placeholder="${messages.getString("profilePage.passwordPlaceholder")}"
                />
            </div>
            <div class="mb-3">
                <label for="confirmPass"
                       class="form-label">${messages.getString("profilePage.confirmPassword")}:</label>
                <input
                        type="password"
                        class="form-control"
                        name="confirmPass"
                        id="confirmPass"
                        placeholder="${messages.getString("profilePage.confirmPasswordPlaceholder")}"
                />
            </div>
            <input type="hidden" name="toPage" value="profile">
            <button
                    type="submit"
                    class="btn btn-primary"
                    name="controller"
                    value="profileChange"
            >
                ${messages.getString("profilePage.update")}
            </button>
        </form>
        <form action="${pageContext.request.contextPath}/controller" method="POST">
            <input type="hidden" name="toPage" value="home">
            <button
                    type="submit"
                    class="btn btn-danger"
                    name="controller"
                    value="logOut"
            >
                ${messages.getString("profilePage.logOut")}
            </button>
        </form>
    </div>
</div>

<script
        src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"
></script>
<script
        src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
        crossorigin="anonymous"
></script>
</body>
</html>
